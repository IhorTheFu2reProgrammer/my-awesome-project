let htmlContainer = document.getElementById("html-container")
let textEditor = document.getElementById("text-editor")

textEditor.addEventListener("input", ()=>{
    htmlContainer.innerHTML = textEditor.value
})